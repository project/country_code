<?php

/**
 * @file
 * Installation file for the country code module.
 */

define('COUNTRY_CODE_MINIMUM_PHP', '5.1.0');

/**
 * Implementation of hook_install().
 */
function country_code_install() {
  // Create tables.
  drupal_install_schema('country_code');
  // Set module weight higher than that of i18n module, which contains a conflicting
  // hook_translation_link_alter() implementation.
  $weight = db_result(db_query("SELECT weight FROM {system} WHERE name = 'i18n'"));
  db_query("UPDATE {system} SET weight = %d WHERE name = 'country_code' AND type = 'module'", $weight ? $weight + 5 : 15);
}

/**
 * Implementation of hook_enable().
 *
 * Add the site default country to the country_code_country table.
 */
function country_code_enable() {
  $country = variable_get('site_country_default_country', '');
  if (!empty($country)) {
    $existing = country_code_country_load($country);
    // Can't use drupal_write_record() here.
    if ($existing) {
      if ($existing['enabled'] == 0) {
        $result = db_query("UPDATE {country_code_country} SET enabled = 1 WHERE country = '%s'", $country);
        variable_set('country_code_count', variable_get('country_code_count', 0) + 1);
      }
    }
    else {
      $result = db_query("INSERT INTO {country_code_country} (country, enabled) VALUES('%s', 1)", $country);
      variable_set('country_code_count', variable_get('country_code_count', 0) + 1);
    }
  }
}

/**
 * Implementation of hook_schema().
 */
function country_code_schema() {
  $schema = array();
  $schema['country_code_country'] = array(
    'description' => t('List of all available countries in the system.'),
    'fields' => array(
      'country' => array(
        'type' => 'varchar',
        'length' => 12,
        'not null' => TRUE,
        'default' => '',
        'description' => t("Country code, e.g. 'us' or 'br'."),
      ),
      'enabled' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Whether the country is enabled.'),
      ),
    ),
    'primary key' => array('country'),
  );

  return $schema;
}

function country_code_update_1() {
  $return = array();

  db_drop_table($return, 'country_code_country_language');

  return $return;
}

function country_code_update_2() {
  $return = array();
  // Set module weight higher than that of i18n module, which contains a conflicting
  // hook_translation_link_alter() implementation.
  $weight = db_result(db_query("SELECT weight FROM {system} WHERE name = 'i18n'"));
  $return[] = update_sql("UPDATE {system} SET weight = " . ($weight ? $weight + 5 : 15) . " WHERE name = 'country_code' AND type = 'module'");
  return $return;
}
/**
 * Implementation of hook_uninstall().
 */
function country_code_uninstall() {
  // Remove tables.
  drupal_uninstall_schema('country_code');
  // Delete variables.
  $variables = array(
    'country_code_browser_language',
    'country_code_count',
    'country_code_external',
    'country_code_fallback',
    'country_code_country_default',
    'country_code_user_country',
    'country_code_user_language',
  );
  foreach ($variables as $variable) {
    variable_del($variable);
  }
}

/**
 * Implementation of hook_disable().
 *
 * Reset the language_negotiation variable to LANGUAGE_NEGOTIATION_NONE.
 */
function country_code_disable() {
  if (variable_get('language_negotiation', LANGUAGE_NEGOTIATION_NONE) == LANGUAGE_NEGOTIATION_COUNTRY_CODE) {
    variable_set('language_negotiation', LANGUAGE_NEGOTIATION_NONE);
    drupal_set_message(t("Your language negotiation setting has been returned to the default value of 'none'."));
  }
}

/**
 * Implementation of hook_requirements().
 *
 * Country code uses array_intersect_key(), not available before PHP 5.
 */
function country_code_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time.
  $t = get_t();

  // Test PHP version.
  $requirements['php'] = array(
    'title' => $t('PHP'),
    'value' => ($phase == 'runtime') ? l(phpversion(), 'admin/logs/status/php') : phpversion(),
  );
  if (version_compare(phpversion(), COUNTRY_CODE_MINIMUM_PHP) < 0) {
    $requirements['php']['description'] = $t('Your PHP installation is too old. Country code requires at least PHP %version.', array('%version' => COUNTRY_CODE_MINIMUM_PHP));
    $requirements['php']['severity'] = REQUIREMENT_ERROR;
  }

  return $requirements;
}

