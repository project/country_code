<?php

/**
 * @file
 * Implementation of a views current country filter for the country code module.
 */

/**
 * Filter by country.
 */
class country_code_handler_filter_node_country_current extends views_handler_filter {

  function query() {
    $this->ensure_my_table();

    $definition = array(
      'table' => 'node',
      'field' => 'tnid',
      'left_table' => 'node',
      'left_field' => 'tnid',
      'extra' => array(
        array(
          'field' => 'language',
          'value' => '***CURRENT_LANGUAGE***',
        ),
      ),
    );

    $join = new views_join();
    $join->definition = $definition;
    $join->construct();

    $alias = $this->query->add_table('node', NULL, $join);
    $this->query->add_where($this->options['group'], "$this->table_alias.language =
'%s' OR ($this->table_alias.language =
'%s' AND $alias.nid IS NULL)", array('***CURRENT_LANGUAGE***', '***COUNTRY_CODE_LANGUAGE***'));
  }

}
