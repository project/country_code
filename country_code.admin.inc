<?php

/**
 * @file
 * Admin page callbacks for the country_code module.
 */

/**
 * User interface for the country overview screen.
 */
function country_code_admin_overview(&$form_state) {
  $countries = country_code_countries(FALSE);

  $options = array();
  $enabled = array();
  $form = array();
  foreach ($countries as $code => $country) {
    $options[$code] = '';
    if ($country->enabled) {
      $enabled[] = $code;
    }
    $form['name'][$code] = array(
      '#value' => check_plain($country->name),
    );
    $languages = array();
    foreach (country_code_languages($code, FALSE) as $language) {
      $languages[] = $language->name;
    }
    $form['language'][$code] = array(
      '#value' => implode(', ', $languages),
    );
  }
  $form['enabled'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $enabled,
  );
  $form['site_default'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => variable_get('site_country_default_country', ''),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Theme the country overview form.
 *
 * @ingroup themeable
 */
function theme_country_code_admin_overview($form) {
  if (isset($form['name']) && is_array($form['name'])) {
    foreach ($form['name'] as $key => $element) {
      if ($key == variable_get('site_country_default_country', '')) {
        $form['enabled'][$key]['#attributes']['disabled'] = 'disabled';
      }
      // Do not take form control structures.
      if (is_array($element) && element_child($key)) {
        $delete = l(t('delete'), 'admin/settings/country-code/delete/'. $key);
        // Remove delete button if it's the last (and hence default) country.
        if (count(country_code_countries(FALSE)) == 1) {
          $delete = '';
        }
        $rows[] = array(
          drupal_render($form['enabled'][$key]),
          drupal_render($form['name'][$key]),
          drupal_render($form['site_default'][$key]),
          drupal_render($form['language'][$key]),
          l(t('edit'), 'admin/settings/country-code-country/'. $key) . ' ' .  $delete,
        );
      }
    }
  }
  else {
    $rows[] = array(array('data' => t('No countries available.'), 'colspan' => '5'));
  }

  $header = array(
    array('data' => t('Enabled')),
    array('data' => t('Country')),
    array('data' => t('Default')),
    array('data' => t('Languages')),
    array('data' => t('Operations')),
  );

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

/**
 * Process country overview form submissions, updating existing countries.
 */
function country_code_admin_overview_submit($form, &$form_state) {
  $countries = country_code_countries(FALSE);
  $enabled_count = 0;
  foreach ($countries as $code => $country) {
    if ($form_state['values']['site_default'] == $code || $code == variable_get('site_country_default_country', '')) {
      // Automatically enable the default country and the country
      // which was default previously (because we will not get the
      // value from that disabled checkox).
      $form_state['values']['enabled'][$code] = 1;
    }
    if ($form_state['values']['enabled'][$code]) {
      $enabled_count++;
      $country->enabled = 1;
    }
    else {
      $country->enabled = 0;
      // If we're disabling the active country, reset cached data so we won't end up
      // at a now-inaccessible path.
      if ($code == country_code()) {
        _country_code_reset();
      }
    }
    db_query("UPDATE {country_code_country} SET enabled = %d WHERE country = '%s'", $country->enabled, $code);
    $countries[$code] = $country;
  }
  variable_set('site_country_default_country', $form_state['values']['site_default']);
  variable_set('country_code_count', $enabled_count);
  drupal_set_message(t('Configuration saved.'));
  $form_state['redirect'] = 'admin/settings/country-code';
  return;
}

/**
 * Add country form.
 */
function country_code_admin_country_form(&$form_state, $country = NULL) {
  $form = array();

  if (isset($country)) {
    $form['country'] = array(
      '#type' => 'value',
      '#value' => $country['country'],
    );
    drupal_set_title(t('Editing country %country', array('%country' => country_code_country_name($country['country']))));
  }
  else {
    // Filter out countries already added.
    $options = array_diff_key(site_country_country_list(), country_code_countries(FALSE));

    $form['country'] = array(
      '#type' => 'select',
      '#title' => t('Country'),
      '#options' => $options,
      '#description' => t('Select the desired country from the list.'),
    );
  }

  $default_value = isset($country['enabled']) ? $country['enabled'] : TRUE;
  $disabled = (variable_get('site_country_default_country', '') == $country['country']) ? TRUE : FALSE;
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $default_value,
    '#disabled' => $disabled,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($country['country']) ? t('Save') : t('Add country'),
  );

  return $form;
}

/**
 * Add country form submit.
 */
function country_code_admin_country_form_submit($form, &$form_state) {
  $flag = country_code_country_save($form_state['values']);
  $variables = array('%country' => country_code_country_name($form_state['values']['country']));
  $message = ($flag == SAVED_NEW ? t('Country %country successfully added.', $variables) : t('Country %country successfully updated.', $variables));
  $form_state['redirect'] = 'admin/settings/country-code';
  return;
}

/**
 * Confirm country deletion.
 */
function country_code_admin_country_delete(&$form_state, $country) {
  $form['country'] = array(
    '#type' => 'value',
    '#value' => $country,
  );

  return confirm_form($form, t('Are you sure you want to delete the country %name?', array('%name' => $country['name'])), 'admin/settings/country-code', t('Deleting a country will remove all settings associated with it. This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process country deletion submissions.
 */
function country_code_admin_country_delete_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $status = country_code_country_delete($form_state['values']['country']);
    if ($status == 1) {
      drupal_set_message(t('The country %country has been removed.', array('%country' => $form_state['values']['country']['name'])));
    }
    elseif ($status == 0) {
      drupal_set_message(t('The country %country could not be removed as it is the default and last country.', array('%country' => $form_state['values']['country']['name'])));
    }
  }
  $form_state['redirect'] = 'admin/settings/country-code';
  return;
}

function country_code_admin_country_languages(&$form_state, $country) {
  $languages = country_code_languages($country['country'], FALSE);

  $form['country'] = array(
    '#type' => 'value',
    '#value' => $country['country'],
  );

  $form['languages']['#tree'] = TRUE;

  foreach ($languages as $lang_code => $language) {
    $form['languages'][$lang_code] = _country_code_country_languages_language($country, $language);
  }

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#disabled' => empty($languages),
  );

  return $form;
}

function _country_code_country_languages_language($country, $language) {
  $form['language'] = array(
    '#type' => 'hidden',
    '#value' => $language->language,
  );
  $form['name'] = array(
    '#type' => 'markup',
    '#value' => $language->name,
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#default_value' => $language->weight,
  );
  $form['operations'] = array(
    '#type' => 'markup',
    '#value' => l(t('delete'), 'admin/settings/language/delete/' . $language->language, array('query' => drupal_get_destination())),
  );

  return $form;
}

function country_code_admin_country_languages_submit($form, &$form_state) {
  foreach ($form_state['values']['languages'] as $language) {
    db_query("UPDATE {languages} SET weight = %d WHERE language = '%s'", $language['weight'], $language['language']);
  }
  $form_state['redirect'] = 'admin/settings/country-code-country/' . $form_state['values']['country'] . '/languages';
  return;
}

function theme_country_code_admin_country_languages($form) {
  $rows = array();
  foreach (element_children($form['languages']) as $key) {
    $row = array();

    $row[] = drupal_render($form['languages'][$key]['language']) . drupal_render($form['languages'][$key]['name']);

    $form['languages'][$key]['weight']['#attributes']['class'] = 'country-code-language-weight';
    $row[] = drupal_render($form['languages'][$key]['weight']);

    $row[] = drupal_render($form['languages'][$key]['operations']);

    $rows[] = array(
      'data' => $row,
      'class' => 'draggable',
    );
  }

  if (count($rows) == 0) {
    $rows[] = array(t('No languages have been added.'), '<span class="country-code-language-weight"></span>', '');
  }

  $header = array(t('Language'), t('Weight'), t('Operations'));
  $output = theme('table', $header, $rows, array('id' => 'country-code-languages'));
  $output .= drupal_render($form);

  drupal_add_tabledrag('country-code-languages', 'order', 'self', 'country-code-language-weight');

  return $output;
}

/**
 * Add language form.
 */
function country_code_admin_language_form(&$form_state, $country = NULL) {
  $form = array();

  $language_codes = country_code_languages($country['country'], FALSE, COUNTRY_CODE_LANGUAGE_SHORT);

  // Filter to show only the languages not already added.
  $options = array_diff_key(country_code_languages_short(), $language_codes);
  if (empty($options)) {
    drupal_set_message(t('No languages were found that have not already been added to the country. You may add a new language on the <a href="@language-add">add language screen</a>.', array('@language-add' => url('admin/settings/language/add'))), 'warning');
    return $form;
  }
  $form['country'] = array(
    '#type' => 'value',
    '#value' => $country['country'],
  );

  $default_language = language_default();

  $form['language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#default_value' => isset($country) ? $language['language'] : NULL,
    '#options' => $options,
    '#required' => TRUE,
    '#description' => t("Select the desired language from the list. When you add a language, a new language will be created. For example, if you add %language_name, you will get a language %country_language_code (%country_language_name).", array('%language_name' => $default_language->name, '%country_language_code' => country_code_get_country_language($default_language->language, $country['country']), '%country_language_name' => t('@language_name (@country_name)', array('@language_name' => $default_language->name, '@country_name' => $country['name'])))),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add language'),
  );

  return $form;
}

/**
 * Add language form submit.
 */
function country_code_admin_language_form_submit($form, &$form_state) {
  // Custom language creation.

  $langcode = country_code_get_country_language($form_state['values']['language'], $form_state['values']['country'], FALSE);
  // Only create if the language doesn't exist.
  if (!country_code_language_exists($langcode)) {
    $languages = language_list();
    $language = $languages[$form_state['values']['language']];
    // Load the language this language is based on.
    $custom_language = array(
      'langcode' => $langcode,
      'name' => $language->name . ' (' . country_code_country_name($form_state['values']['country']) . ')',
      'native' => $language->native . ' (' . $form_state['values']['country'] . ')',
      'direction' => $language->direction,
      'domain' => '',
      'prefix' => strtolower($langcode),
    );
    locale_inc_callback('locale_add_language', $custom_language['langcode'], $custom_language['name'], $custom_language['native'], $custom_language['direction'], $custom_language['domain'], $custom_language['prefix']);
    drupal_set_message(t('The language %language has been created and can now be used. You may customize this language on the <a href="@language-edit">language edit screen</a>.', array('%language' => t($custom_language['name']), '@language-edit' => url('admin/settings/language/edit/' . $langcode))));
  }

  $form_state['redirect'] = 'admin/settings/country-code-country/' . $form_state['values']['country'] . '/languages';
  return;
}

/**
 * Menu callback; presents the country_code settings page.
 */
function country_code_admin_settings() {
  $form = array();

  $form['country'] = array(
    '#type' => 'fieldset',
    '#title' => t('Country settings'),
    '#collapsible' => TRUE,
  );
  $form['country']['country_code_fallback'] = array(
    '#type' => 'radios',
    '#title' => t('Country fallback option'),
    '#default_value' => variable_get('country_code_fallback', COUNTRY_CODE_GLOBAL),
    '#options' => array(COUNTRY_CODE_GLOBAL => t('Global'), COUNTRY_CODE_DEFAULT => t('Default')),
    '#description' => t('Default setting to use when no user country is detected, or where the user country is not among the enabled countries. Select "Global" to show a global version of the site (with no country-specific paths, filtering, or display). Select "Default" to use the default country.'),
  );

  $form['country']['country_code_user_country'] = array(
    '#type' => 'radios',
    '#title' => t('User default country'),
    '#default_value' => variable_get('country_code_user_country', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('Enable this option to allow users to select a default country on their user profile edit form.'),
  );

  $form['country']['country_code_external'] = array(
    '#type' => 'radios',
    '#title' => t('Set country by path for initial or external links'),
    '#default_value' => variable_get('country_code_external', 0),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('Enable this option to have the country determined through the path prefix when the site is first visited or is visited from other sites. Disable this option to have the default country selection used (based on user preferred country or IP detection).'),
  );

  $handlers = module_invoke_all('country_code_handler');
  // Only show element for selecting a handler if there is more than one
  // to select from.
  if (count($handlers) > 1) {
    $options = array();
    foreach ($handlers as $code => $handler) {
      $options[$code] = $handler['title'];
    }

    $form['country']['country_code_handler'] = array(
      '#type' => 'radios',
      '#title' => t('Set country by path for initial or external links'),
      '#default_value' => variable_get('country_code_handler', 'country_code_hostip'),
      '#options' => $options,
      '#description' => t('Select a handler for country code determination based on IP.'),
    );
  }

  $form['language'] = array(
    '#type' => 'fieldset',
    '#title' => t('Language settings'),
    '#collapsible' => TRUE,
  );

  $form['language']['country_code_user_language'] = array(
    '#type' => 'radios',
    '#title' => t('User default language'),
    '#default_value' => variable_get('country_code_user_language', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('Enable this option to allow users to select a default language on their user profile edit form. If both this option and <em>User default country</em> are enabled, users will be offered the languages for the country they have selected.'),
  );

  $form['language']['country_code_browser_language'] = array(
    '#type' => 'radios',
    '#title' => t('Consider user browser language'),
    '#default_value' => variable_get('country_code_browser_language', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t("Select this option to have the user's browser language set as the language, if that language is available for the user's country."),
  );

  $form['links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Links'),
    '#collapsible' => TRUE,
  );
  $form['links']['country_code_node_links'] = array(
    '#type' => 'radios',
    '#title' => t('Show translations in node links'),
    '#default_value' => variable_get('country_code_node_links', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('Choose whether to show links to translations in node links.'),
  );

  return system_settings_form($form);
}
