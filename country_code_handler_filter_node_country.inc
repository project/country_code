<?php

/**
 * @file
 * Implementation of a views country filter for the country code module.
 */

/**
 * Filter by country.
 */
class country_code_handler_filter_node_country extends views_handler_filter_in_operator {

  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Country');
      $countries = array(
        '***COUNTRY_CODE***' => t("Current country"),
      );
      foreach (country_code_countries() as $code => $country) {
        $countries[$code] = $country->name;
      }
      $this->value_options = $countries;
    }
  }

  function query() {
    // If the current country code is global, we don't restrict content.
    if (empty($this->value) || in_array(COUNTRY_CODE_GLOBAL, $this->value)) {
      return;
    }

    $table = $this->ensure_my_table();
    $language_substrings = array();
    $options = array();
    foreach ($this->value as $value) {
      // The search strings include a dash(-), e.g., -ca, so we begin at the 3rd
      // character.
      $options[] = "SUBSTRING({$this->table_alias}.{$this->real_field}, 3) = '%s'";
      $language_substrings[] = '-' . $value;
    }

    $where = ' (' . implode(" OR ", $options) . ')';

    $this->query->add_where($this->options['group'], $where, $language_substrings);
  }

}
