<?php

/**
 * @file
 * Field handler for country code module.
 */

/**
 * Field handler to translate a node language into a readable form of its country.
 */
class country_code_handler_field_node_country extends views_handler_field_node {
  function construct() {
    parent::construct();
    $this->additional_fields['language'] = 'language';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $language = $values->{$this->aliases['language']};
    if ($country_code = strtolower(substr($language, 3))) {
      if ($value = country_code_country_name($country_code)) {
        return $this->render_link(check_plain($value), $values);
      }
    }
    return '';
  }
}

