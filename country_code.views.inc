<?php

/**
 * @file
 * Views handling file for country code module.
 */

/**
 * Implementation of hook_views_handlers().
 */
function country_code_views_handlers() {
  return array(
    'handlers' => array(
      'country_code_handler_field_node_country' => array(
        'parent' => 'views_handler_field_node',
      ),
      'country_code_handler_filter_node_country' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
      'country_code_handler_filter_node_country_current' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

/**
 * Substitute country code and country language; this works with cached queries.
 */
function country_code_views_query_substitutions($view) {
  global $language;
  return array(
    '***COUNTRY_CODE***' => country_code(),
    // Pass a two-digit language, to be used as a fallback.
    '***COUNTRY_CODE_LANGUAGE***' => substr($language->language, 0, 2),
  );
}

/**
 * Implementation of hook_views_data_alter().
 *
 * Add country information to the node table.
 */
function country_code_views_data_alter(&$data) {

  // Only add fields if translation module enabled.
  if (module_exists('translation')) {
    // Country field.
    $data['node']['country_code'] = array(
      'title' => t('Country'),
      'help' => t('The country the content is set to.'),
      'field' => array(
        'title' => t('Country'),
        'help' => t('Display the country based on langauge.'),
        'handler' => 'country_code_handler_field_node_country',
      ),
      'filter' => array(
        'field' => 'language',
        'handler' => 'country_code_handler_filter_node_country',
        'label' => t('Country'),
      ),
    );
    // Current country field.
    $data['node']['country_code_current'] = array(
      'title' => t("Current country's language"),
      'help' => t("Content for the current country language, with a generic language fallback. If the current language is fr-ca for Canadian French, this filter will show content in the fr-ca language or, if there isn't such a translation, a fr version."),
      'filter' => array(
        'field' => 'language',
        'handler' => 'country_code_handler_filter_node_country_current',
        'label' => t("Current country's language"),
      ),
    );
  }

}
