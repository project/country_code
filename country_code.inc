<?php

/**
 * @file
 * Include file to be included in hook_boot(). Because the functions here may
 * be run before Drupal is fully bootstrapped, care must be taken not to call
 * module functions.
 *
 * TODO: hook_requirements() for checking for other modules implementing url
 * rewriting functions.
 */

/**
 * Rewrite outbound paths to have a country code prefix.
 */
function custom_url_rewrite_outbound(&$path, &$options, $original_path, $reset = FALSE) {
  static $module_exists, $country_codes, $country_code_prefix;

  if ($reset || !isset($module_exists)) {
    $module_exists = module_exists('country_code');
    $country_codes = array_keys(country_code_countries());
    $country_code_prefix = _country_code_prefix();
  }
  // Only modify relative (insite) URLs.
  if ($module_exists && !$options['external']) {
    // Don't modify links that already have a country prefix.
    $args = explode('/', $path);
    $prefix = array_shift($args);
    // If there's a global prefix, remove it.
    if ($prefix == COUNTRY_CODE_GLOBAL) {
      $path = implode('/', $args);
    }
    elseif (!in_array($prefix, $country_codes)) {
      $options['prefix'] = $country_code_prefix;
    }
  }
}

/**
 * Rewrite inbound paths to remove a country code prefix.
 */
function custom_url_rewrite_inbound(&$result, $path, $path_language, $reset = FALSE) {
  static $module_exists, $prefix;

  if ($reset || !isset($module_exists)) {
    $module_exists = module_exists('country_code');
    $prefix = _country_code_prefix();
  }
  // Only modify paths having the prefix added by custom_url_rewrite_outbound().
  if ($module_exists && substr($path, 0, 3) == $prefix) {
    $result = substr($path, 3);
  }
}
